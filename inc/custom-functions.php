<?php
defined( 'ABSPATH' ) || exit;

// Logo Source
if (!function_exists('ibid_logo_source')) {
    function ibid_logo_source(){
        
        // REDUX VARIABLE
        global $ibid_redux;
        // html VARIABLE
        $html = '';
        // Metaboxes
        $mt_custom_header_options_status = get_post_meta( get_the_ID(), 'ibid_custom_header_options_status', true );
        $mt_metabox_header_logo = get_post_meta( get_the_ID(), 'ibid_metabox_header_logo', true );
        if (is_page()) {
            if (isset($mt_custom_header_options_status) && isset($mt_metabox_header_logo) && $mt_custom_header_options_status == 'yes') {
                $html .='<img src="'.esc_url($mt_metabox_header_logo).'" alt="'.esc_attr(get_bloginfo()).'" />';
            }else{
                if(!empty($ibid_redux['ibid_logo']['url'])){
                    $html .='<img src="'.esc_url($ibid_redux['ibid_logo']['url']).'" alt="'.esc_attr(get_bloginfo()).'" />';
                }else{ 
                    $html .= $ibid_redux['ibid_logo_text'];
                }
            }
        }else{
            if(!empty($ibid_redux['ibid_logo']['url'])){
                $html .='<img src="'.esc_url($ibid_redux['ibid_logo']['url']).'" alt="'.esc_attr(get_bloginfo()).'" />';
            }elseif(isset($ibid_redux['ibid_logo_text'])){ 
                $html .= $ibid_redux['ibid_logo_text'];
            }else{
                $html .= esc_html(get_bloginfo());
            }
        }
        return $html; 
    }
}
// Logo Area
if (!function_exists('ibid_logo')) {
    function ibid_logo(){
    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        global $ibid_redux;
        // html VARIABLE
        $html = '';
        $html .='<div class="logo logo-image">';
            $html .='<a href="'.esc_url(get_site_url()).'">';
                $html .= ibid_logo_source();
            $html .='</a>';
        $html .='</div>';
        return $html;
        // REDUX VARIABLE
     } else {
        global $ibid_redux;
        // html VARIABLE
        $html = '';
        $html .='<div class="logo logo-h">';
            $html .='<a href="'.esc_url(get_site_url()).'">';
                $html .= esc_html(get_bloginfo());
            $html .='</a>';
        $html .='</div>';
        return $html;
     } 
    }
}
// Add specific CSS class by filter
if (!function_exists('ibid_body_classes')) {
    function ibid_body_classes( $classes ) {
        global  $ibid_redux;
        $plugin_redux_status = '';
        if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
            $plugin_redux_status = 'missing-redux-framework';
        }
        $plugin_modeltheme_status = '';
        if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
            $plugin_modeltheme_status = 'missing-modeltheme-framework';
        }
        // CHECK IF FEATURED IMAGE IS FALSE(Disabled)
        $post_featured_image = '';
        if (is_single()) {
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                if ($ibid_redux['post_featured_image'] == false) {
                    $post_featured_image = 'hide_post_featured_image';
                }else{
                    $post_featured_image = '';
                }
            }
        }
        // CHECK IF THE NAV IS STICKY
        $is_nav_sticky = '';
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            if ($ibid_redux['is_nav_sticky'] == true) {
                // If is sticky
                $is_nav_sticky = 'is_nav_sticky';
            }else{
                // If is not sticky
                $is_nav_sticky = '';
            }
        }
        // CHECK IF THE NAV IS STICKY
        $is_category_menu = '';
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            if ($ibid_redux['ibid_header_category_menu_mobile'] == true) {
                // If is sticky
                $is_category_menu = 'is_category_menu';
            }else{
                // If is not sticky
                $is_category_menu = '';
            }
        }
        // DIFFERENT HEADER LAYOUT TEMPLATES
        $header_version = 'first_header';
        if (is_page()) {
            $mt_custom_header_options_status = get_post_meta( get_the_ID(), 'ibid_custom_header_options_status', true );
            $mt_header_custom_variant = get_post_meta( get_the_ID(), 'ibid_header_custom_variant', true );
            $header_version = 'first_header';
            if (isset($mt_custom_header_options_status) AND $mt_custom_header_options_status == 'yes') {
                if ($mt_header_custom_variant == '1') {
                    // Header Layout #1
                    $header_version = 'first_header';
                }elseif ($mt_header_custom_variant == '2') {
                    // Header Layout #2
                    $header_version = 'second_header';
                }elseif ($mt_header_custom_variant == '3') {
                    // Header Layout #3
                    $header_version = 'third_header';
                }elseif ($mt_header_custom_variant == '4') {
                    // Header Layout #4
                    $header_version = 'fourth_header';
                }elseif ($mt_header_custom_variant == '5') {
                    // Header Layout #5
                    $header_version = 'fifth_header';
                }elseif ($mt_header_custom_variant == '6') {
                    // Header Layout #6
                    $header_version = 'sixth_header';
                }elseif ($mt_header_custom_variant == '7') {
                    // Header Layout #7
                    $header_version = 'seventh_header';
                }elseif ($mt_header_custom_variant == '8') {
                    // Header Layout #8
                    $header_version = 'eighth_header';
                }elseif ($mt_header_custom_variant == '9') {
                    // Header Layout #8
                    $header_version = 'ninth_header';
                }else{
                    // if no header layout selected show header layout #1
                    $header_version = 'first_header';
                }
            }else{
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    if ($ibid_redux['header_layout'] == 'first_header') {
                        // Header Layout #1
                        $header_version = 'first_header';
                    }elseif ($ibid_redux['header_layout'] == 'second_header') {
                        // Header Layout #2
                        $header_version = 'second_header';
                    }elseif ($ibid_redux['header_layout'] == 'third_header') {
                        // Header Layout #3
                        $header_version = 'third_header';
                    }elseif ($ibid_redux['header_layout'] == 'fourth_header') {
                        // Header Layout #4
                        $header_version = 'fourth_header';
                    }elseif ($ibid_redux['header_layout'] == 'fifth_header') {
                        // Header Layout #5
                        $header_version = 'fifth_header';
                    }elseif ($ibid_redux['header_layout'] == 'sixth_header') {
                        // Header Layout #6
                        $header_version = 'sixth_header';
                    }elseif ($ibid_redux['header_layout'] == 'seventh_header') {
                        // Header Layout #7
                        $header_version = 'seventh_header';
                    }elseif ($ibid_redux['header_layout'] == 'eighth_header') {
                        // Header Layout #8
                        $header_version = 'eighth_header';
                    }elseif ($ibid_redux['header_layout'] == 'ninth_header') {
                        // Header Layout #8
                        $header_version = 'ninth_header';
                    }else{
                        // if no header layout selected show header layout #1
                        $header_version = 'first_header';
                    }
                }
            }
        }else{
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                if ($ibid_redux['header_layout'] == 'first_header') {
                    // Header Layout #1
                    $header_version = 'first_header';
                }elseif ($ibid_redux['header_layout'] == 'second_header') {
                    // Header Layout #2
                    $header_version = 'second_header';
                }elseif ($ibid_redux['header_layout'] == 'third_header') {
                    // Header Layout #3
                    $header_version = 'third_header';
                }elseif ($ibid_redux['header_layout'] == 'fourth_header') {
                    // Header Layout #4
                    $header_version = 'fourth_header';
                }elseif ($ibid_redux['header_layout'] == 'fifth_header') {
                    // Header Layout #5
                    $header_version = 'fifth_header';
                }elseif ($ibid_redux['header_layout'] == 'sixth_header') {
                    // Header Layout #6
                    $header_version = 'sixth_header';
                }elseif ($ibid_redux['header_layout'] == 'seventh_header') {
                    // Header Layout #7
                    $header_version = 'seventh_header';
                }elseif ($ibid_redux['header_layout'] == 'eighth_header') {
                    // Header Layout #8
                    $header_version = 'eighth_header';
                }elseif ($ibid_redux['header_layout'] == 'ninth_header') {
                    // Header Layout #8
                    $header_version = 'ninth_header';
                }else{
                    // if no header layout selected show header layout #1
                    $header_version = 'first_header';
                }
            }
        }

        $wc_vendors_status = '';
        if (class_exists('WC_Vendors')) {
            $wc_vendors_status = 'wc_vendors_active';
        }


        $mt_footer_row1 = '';
        $mt_footer_row2 = '';
        $mt_footer_row3 = '';
        $mt_footer_row4 = '';
        $mt_footer_bottom = '';
        
        $mt_footer_row1_status = get_post_meta( get_the_ID(), 'mt_footer_row1_status', true );
        $mt_footer_row2_status = get_post_meta( get_the_ID(), 'mt_footer_row2_status', true );
        $mt_footer_row3_status = get_post_meta( get_the_ID(), 'mt_footer_row3_status', true );
        $mt_footer_bottom_bar = get_post_meta( get_the_ID(), 'mt_footer_bottom_bar', true );

        if (isset($mt_footer_row1_status) && !empty($mt_footer_row1_status)) {
            $mt_footer_row1 = 'hide-footer-row-1';
        }
        if (isset($mt_footer_row2_status) && !empty($mt_footer_row2_status)) {
            $mt_footer_row2 = 'hide-footer-row-2';
        }
        if (isset($mt_footer_row3_status) && !empty($mt_footer_row3_status)) {
            $mt_footer_row3 = 'hide-footer-row-3';
        }
        if (isset($mt_footer_bottom_bar) && !empty($mt_footer_bottom_bar)) {
            $mt_footer_bottom = 'hide-footer-bottom';
        }


        $classes[] = esc_attr($mt_footer_row1) . ' ' . esc_attr($mt_footer_row2) . ' ' . esc_attr($mt_footer_row3) . ' ' . esc_attr($mt_footer_bottom) . ' ' . esc_attr($wc_vendors_status) . ' ' . esc_attr($plugin_modeltheme_status) . ' ' . esc_attr($plugin_redux_status) . ' ' . esc_attr($is_nav_sticky) . ' ' . esc_attr($is_category_menu) . ' ' . esc_attr($header_version) . ' ' . esc_attr($post_featured_image) . ' ';

        return $classes;
    }
    add_filter( 'body_class', 'ibid_body_classes' );
}


// Mobile Dropdown Menu Button
if (!function_exists('ibid_burger_dropdown_button')) {
    function ibid_burger_dropdown_button(){
        if ( !class_exists( 'mega_main_init' ) ) {
        echo'<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>';
        }
    }
    add_action('ibid_burger_dropdown_button', 'ibid_burger_dropdown_button');
}


// Mobile Burger Aside variant
if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
    if ($ibid_redux['ibid_mobile_burger_select'] == 'sidebar') {
        if (!function_exists('ibid_burger_aside_button')) {
            function ibid_burger_aside_button(){
                if ( !class_exists( 'mega_main_init' ) ) { 
                    echo '<button id="aside-menu" type="button" class="navbar-toggle" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>';
                }
            }
        add_action('ibid_before_mobile_navigation_burger', 'ibid_burger_aside_button');
        }
    }
}

if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
    if ($ibid_redux['ibid_mobile_burger_select'] == 'sidebar') {
        if (!function_exists('ibid_burger_aside_menu')) {
            function ibid_burger_aside_menu(){

                global $ibid_redux;
                if( function_exists( 'YITH_WCWL' ) ){
                    $wishlist_url = YITH_WCWL()->get_wishlist_url();
                }else{
                    $wishlist_url = '#';
                }

                echo'<div class="mt-header">
                        <div class="header-aside">
                            <div class="aside-navbar">
                                <div class="aside-tabs">
                                    <a href="#mt-first-menu">'.esc_html__('Menu','ibid').'</a>
                                    <a href="#mt-second-menu">'.esc_html__('Categories','ibid').'</a>
                                </div>
                                <div class="nav-title">'.esc_html__('Menu','ibid').'</div>
                                    <div class="mt-nav-content">
                                        <div class="mt-first-menu">
                                            <div class="bot_nav_wrap">
                                                <ul class="menu nav navbar-nav pull-left nav-effect nav-menu">';
                                                    if ( has_nav_menu( 'primary' ) ) {
                                                    $defaults = array(
                                                        'menu'            => '',
                                                        'container'       => false,
                                                        'container_class' => '',
                                                        'container_id'    => '',
                                                        'menu_class'      => 'menu',
                                                        'menu_id'         => '',
                                                        'echo'            => true,
                                                        'fallback_cb'     => false,
                                                        'before'          => '',
                                                        'after'           => '',
                                                        'link_before'     => '',
                                                        'link_after'      => '',
                                                        'items_wrap'      => '%3$s',
                                                        'depth'           => 0,
                                                        'walker'          => ''
                                                    );
                                                    $defaults['theme_location'] = 'primary';
                                                    wp_nav_menu( $defaults );
                                                    }else{
                                                    echo '<p class="no-menu text-right">';
                                                        echo esc_html__('Primary navigation menu is missing.', 'ibid');
                                                    echo '</p>';
                                                    }   
                                                echo '</ul>
                                            </div>
                                        </div>
                                        <div class="mt-second-menu">';

                                        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                                        echo'<div class="bot_nav_cat_inner">
                                                <div class="bot_nav_cat">
                                                    <ul class="bot_nav_cat_wrap">';
                                                    if ( has_nav_menu( 'category' ) ) {
                                                        $defaults = array(
                                                            'menu'            => '',
                                                            'container'       => false,
                                                            'container_class' => '',
                                                            'container_id'    => '',
                                                            'menu_class'      => 'menu',
                                                            'menu_id'         => '',
                                                            'echo'            => true,
                                                            'fallback_cb'     => false,
                                                            'before'          => '',
                                                            'after'           => '',
                                                            'link_before'     => '',
                                                            'link_after'      => '',
                                                            'items_wrap'      => '%3$s',
                                                            'depth'           => 0,
                                                            'walker'          => ''
                                                        );
                                                        $defaults['theme_location'] = 'category';
                                                        wp_nav_menu( $defaults );
                                                    }else{
                                                        echo '<p class="no-menu text-right">';
                                                            echo esc_html__('Category navigation menu is missing.', 'ibid');
                                                        echo '</p>';
                                                    }
                                            echo'</ul>
                                            </div>
                                        </div>';
                                       }
                                    echo '</div>
                                    </div>
                                    <div class="aside-footer">';
                                        if (isset($ibid_redux['ibid_top_header_order_tracking_link']) && $ibid_redux['ibid_top_header_order_tracking_link'] != '') {
                                            echo '<a class="top-order" href="'.esc_url($ibid_redux['ibid_top_header_order_tracking_link']).'">
                                                <i class="fa fa-truck"></i>'.esc_html__('Order Tracking', 'ibid').'</a>';
                                        }
                                        if( function_exists( 'YITH_WCWL' ) ){
                                            echo '<a class="top-payment" href="'.esc_url($wishlist_url).'">
                                            <i class="fa fa-heart-o"></i>'.esc_html__('Wishlist', 'ibid').'</a>';
                                        }
                                    echo '</div>
                                </div>
                            </div>
                        </div>';
                echo '<div class="aside-bg"></div>';
            }
    add_action('ibid_after_mobile_navigation_burger', 'ibid_burger_aside_menu');
    }
}}


// Mobile Icons Top Group
if (!function_exists('ibid_header_mobile_icons_group')) {
    function ibid_header_mobile_icons_group(){

        if ( class_exists( 'ReduxFrameworkPlugin' ) ) { 
            if (ibid_redux('ibid_header_mobile_switcher_top') == true) {

                $cart_url = "#";
                if ( class_exists( 'WooCommerce' ) ) {
                    $cart_url = wc_get_cart_url();
                }
                #YITH Wishlist rul
                if( function_exists( 'YITH_WCWL' ) ){
                    $wishlist_url = YITH_WCWL()->get_wishlist_url();
                }else{
                    $wishlist_url = '#';
                }

                if (ibid_redux('ibid_header_mobile_switcher_top_search') == true) {
                    echo '<div class="mobile_only_icon_group search">
                                <a href="#" class="mt-search-icon">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </div>';
                }
                if (ibid_redux('ibid_header_mobile_switcher_top_cart') == true) {
                    echo '<div class="mobile_only_icon_group cart">
                                <a  href="' .esc_url($cart_url).'">
                                    <i class="fa fa-shopping-basket"></i>
                                </a>
                            </div>';
                }
                if (ibid_redux('ibid_header_mobile_switcher_top_wishlist') == true) {
                    echo '<div class="mobile_only_icon_group wishlist">
                                <a class="top-payment" href="'.esc_url($wishlist_url).'">
                                  <i class="fa fa-heart-o"></i>
                                </a>
                            </div>';
                }

                if(ibid_redux('is_popup_enabled') == true) {
                    if (is_user_logged_in() || is_account_page()) {
                        $user_url = get_permalink( get_option('woocommerce_myaccount_page_id') );;
                        $data_attributes = '';
                    }else{
                        $user_url = '#';
                        $data_attributes = 'data-modal="modal-log-in" class="modeltheme-trigger"';
                    }
                }else{
                    $user_url = get_permalink( get_option('woocommerce_myaccount_page_id') );;
                    $data_attributes = '';
                }

                if (ibid_redux('ibid_header_mobile_switcher_top_account') == true) {
                    echo '<div class="mobile_only_icon_group account">
                                <a href="' .esc_url($user_url). '" '.wp_kses_post($data_attributes).'>
                                    <i class="fa fa-user"></i>
                                </a>
                        </div>';
               }
            }
        }

    }
    add_action('ibid_before_mobile_navigation_burger', 'ibid_header_mobile_icons_group');
}

// Mobile Icons Bottom Group
if (!function_exists('ibid_footer_mobile_icons_group')) {
    function ibid_footer_mobile_icons_group(){

        if ( class_exists( 'ReduxFrameworkPlugin' ) ) { 
            if (ibid_redux('ibid_header_mobile_switcher_footer') == true) {

                $cart_url = "#";
                if ( class_exists( 'WooCommerce' ) ) {
                    $cart_url = wc_get_cart_url();
                }

                #YITH Wishlist rul
                if( function_exists( 'YITH_WCWL' ) ){
                    $wishlist_url = YITH_WCWL()->get_wishlist_url();
                }else{
                    $wishlist_url = '#';
                }
                
                echo '<div class="mobile_footer_icon_wrapper">';
                    if (ibid_redux('ibid_header_mobile_switcher_footer_search') == true) {
                        echo '<div class="col-md-3 search">
                                    <a href="#" class="mt-search-icon">
                                        <i class="fa fa-search" aria-hidden="true"></i>'.esc_html__('Search','ibid').'
                                    </a>
                                </div>';
                    }
                    if (ibid_redux('ibid_header_mobile_switcher_footer_cart') == true) {
                        echo '<div class="col-md-3 cart">
                                    <a  href="' .esc_url($cart_url). '">
                                        <i class="fa fa-shopping-basket" aria-hidden="true"></i>'.esc_html__('Cart','ibid').'
                                    </a>
                                </div>';
                    }
                    if (ibid_redux('ibid_header_mobile_switcher_footer_wishlist') == true) {
                        echo '<div class="col-md-3 wishlist">
                                    <a class="top-payment" href="'  .esc_url($wishlist_url).'">
                                      <i class="fa fa-heart-o"></i>'.esc_html__('Wishlist','ibid').'
                                    </a>
                                </div>';
                    }
                    if (ibid_redux('ibid_header_mobile_switcher_footer_account') == true) {

                        if(ibid_redux('is_popup_enabled') == true) {
                            if (is_user_logged_in() || is_account_page()) {
                                $user_url = get_permalink( get_option('woocommerce_myaccount_page_id') );;
                                $data_attributes = '';
                            }else{
                                $user_url = '#';
                                $data_attributes = 'data-modal="modal-log-in" class="modeltheme-trigger"';
                            }
                        }else{
                            $user_url = get_permalink( get_option('woocommerce_myaccount_page_id') );;
                            $data_attributes = '';
                        }

                        echo '<div class="col-md-3 account">
                                    <a href="' .esc_url($user_url). '" '.wp_kses_post($data_attributes).'>
                                      <i class="fa fa-user"></i>'.esc_html__('Account','ibid').'
                                    </a>
                                </div>';
                    }
                echo '</div>';
            }
        }
    }
    add_action('ibid_before_footer_mobile_navigation', 'ibid_footer_mobile_icons_group');
}

// Top Header Banner
if (!function_exists('ibid_my_banner_header')) {
    function ibid_my_banner_header() {
        echo '<div class="ibid-top-banner text-center">
                    <span class="discount-text">'.ibid_redux('discout_header_text').'</span>
                    <div class="text-center row">';
                    echo do_shortcode('[shortcode_countdown_v2 insert_date="'.ibid_redux('discout_header_date').'"]');
              echo '</div>
              <a class="button btn" href="'.ibid_redux('discout_header_btn_link').'">'.ibid_redux('discout_header_btn_text').'</a>
              </div>';
    }
}

//GET HEADER TITLE/BREADCRUMBS AREA
if (!function_exists('ibid_header_title_breadcrumbs')) {
    function ibid_header_title_breadcrumbs(){
        echo '<div class="ibid-breadcrumbs">';
            echo '<div class="container">';
                echo '<div class="row">';

                    if(!function_exists('bcn_display')){
                        echo '<div class="col-md-12">';
                            echo '<ol class="breadcrumb">';
                                echo ibid_breadcrumb();
                            echo '</ol>';
                        echo '</div>';
                    } else {
                        echo '<div class="col-md-12">';
                            echo '<div class="breadcrumbs breadcrumbs-navxt" typeof="BreadcrumbList" vocab="https://schema.org/">';
                                echo bcn_display();
                            echo '</div>';
                        echo '</div>';
                    }
                    echo '<div class="col-md-12">';
                        if (is_singular('post')) {
                            echo '<h1>'.get_the_title().'</h1>';
                        }elseif (is_singular('cause')) {
                            echo '<h1>'.get_the_title().'</h1>';    
                        }elseif (is_page()) {
                            echo '<h1>'.get_the_title().'</h1>';
                        }elseif (is_singular('product')) {
                            echo '<h1>'.esc_html__( 'Our Shop', 'ibid' ) . get_search_query().'</h1>';
                        }elseif (is_search()) {
                            echo '<h1>'.esc_html__( 'Search Results for: ', 'ibid' ) . get_search_query().'</h1>';
                        }elseif (is_category()) {
                            echo '<h1>'.esc_html__( 'Category: ', 'ibid' ).' <span>'.single_cat_title( '', false ).'</span></h1>';
                        }elseif (is_tag()) {
                            echo '<h1>'.esc_html__( 'Tag: ', 'ibid' ) . single_tag_title( '', false ).'</h1>';
                        }elseif (is_author() || is_archive()) {
                            if (function_exists("is_shop") && is_shop()) {
                            }else{
                                echo '<h1>'.get_the_archive_title().'</h1>';
                            }
                        }elseif (is_home()) {
                            echo '<h1>'.esc_html__( 'From the Blog', 'ibid' ).'</h1>';
                        }
                        
                    echo'</div>';
                    if (is_singular('cause')) {
                        global $ibid_redux;
                        $cause_goal = get_post_meta( get_the_ID(), 'cause_goal', true );
                        if ($cause_goal) {
                            echo '<div class="col-md-12">';
                                echo' <h4>'.esc_html__('Goal: ', 'ibid').'</span>'.esc_html($cause_goal).'</h4>';
                            echo'</div>';
                        }
                    }
                echo'</div>';
            echo'</div>';
        echo'</div>';
    }
}


// Mobile Dropdown Menu Button
if (!function_exists('ibid_get_login_link')) {
    function ibid_get_login_link(){

        if(ibid_redux('is_popup_enabled') == true) {
            if (is_user_logged_in() || is_account_page()) {
                $user_url = get_permalink( get_option('woocommerce_myaccount_page_id') );;
                $data_attributes = '';
            }else{
                $user_url = '#';
                $data_attributes = 'data-modal="modal-log-in" class="modeltheme-trigger"';
            }
        }else{
            $user_url = get_permalink( get_option('woocommerce_myaccount_page_id') );;
            $data_attributes = '';
        }
        ?>

        <a href="<?php echo esc_url($user_url); ?>" <?php echo wp_kses_post($data_attributes); ?>>
            <?php esc_html_e('Sign In','ibid'); ?>
        </a>

        <?php 
    }
    add_action('ibid_login_link_a', 'ibid_get_login_link');
}


/**
*
* Adds the blue donation cause select when editing a product
*
* @since 3.3
*
* @package ibid
*/
if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
    if (ibid_redux('ibid_enable_fundraising') == 'enable') {
        if (!function_exists('ibid_charity_select_cause_form_group')) {
            function ibid_charity_select_cause_form_group($product_id){
                ?>
                <?php
                    //Charity metas
                    $meta_product_cause = get_post_meta( $product_id, 'product_cause', true );
                ?>
                <!-- Charity Cause -->
                <h3 class="ibid-relist-auction"><?php esc_html_e( 'Charity Cause', 'ibid' ); ?></h3>
                <div id="auction_tab" class="panel woocommerce_options_panel" style="display: block;">
                    <div class="row">
                        <div class="col-md-4">
                            <p class=" form-field _auction_item_condition_field">
                                <select id="product_cause" name="product_cause" class="form-control select short">
                                    <option value=""><?php esc_html_e( 'Select a Charity Cause', 'ibid' ); ?></option>
                                    <?php
                                    $cause_posts = get_posts( array( 'post_type' => 'cause', 'posts_per_page' => -1) );
                                    foreach ($cause_posts as $cause_post) {
                                        $selected = '';
                                        if ((isset($meta_product_cause) && !empty($meta_product_cause)) && $meta_product_cause == $cause_post->ID) {
                                            $selected = 'selected';
                                        } ?>
                                        <option <?php echo esc_attr($selected); ?> value="<?php echo esc_attr($cause_post->ID); ?>"><?php echo esc_html($cause_post->post_title); ?></option>
                                    <?php } ?>
                                </select>
                            </p>
                        </div>
                        <div class="col-md-8">
                            <p><i><?php esc_html_e( 'If this auction will be charity auction you can select a cause to support, from the dropdown. Otherwise, leave it unselected.', 'ibid' ); ?></i></p>
                        </div>
                    </div>
                </div>
                <?php 
            }
        }
        add_action('ibid_before_add_auction_form', 'ibid_charity_select_cause_form_group');
    }
}



/**
*
* Adds the blue attachment field on auction edit view
*
* @since 3.3
*
* @package ibid
*/
if (!function_exists('ibid_product_attachment_form_group')) {
    function ibid_product_attachment_form_group($product_id){
        ?>
        <?php 
            //Attach PDF
            $meta_attach_pdf = get_post_meta( $product_id, 'ibid_pdf_attach', true );
        ?>
        <!-- PDF Attachment link -->
        <h4 class="ibid-relist-auction"><?php esc_html_e( 'PDF Attachment link', 'ibid' ); ?></h4>
        <div class="dokan-product-attach-pdf">
            <label for="post_content" class="form-label"><?php esc_html_e( 'Input Attach PDF', 'ibid' ); ?></label>
            <input type="text" class="form-control wc_input_attach_pdf short wc_attach_pdf" style="" name="ibid_pdf_attach" id="ibid_pdf_attach" value="<?php echo esc_attr($meta_attach_pdf); ?>">
        </div>
        <?php 
    }
}
if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
    if (ibid_redux('ibid_auction_attachment_link') == true) {
        add_action('ibid_after_add_auction_form', 'ibid_product_attachment_form_group');
    }
}


/**
*
* Adds the blue auctions box to marketplace plugins such as dokan/wcfm
*
* @since 3.3
*
* @package ibid
*/
if (!function_exists('ibid_custom_auctions_blue_box')) {
    function ibid_custom_auctions_blue_box($product_id){

        $style = 'display: none;';
        if ($product_id) {
            $_product = wc_get_product( $product_id );
            if( $_product->is_type( 'auction' ) ) {
                $style = 'display: block;';
            }
        }

        ?>

        <div class="ibid-auction-settings" style="<?php echo esc_attr($style); ?>">

            <?php do_action('ibid_before_add_auction_form', $product_id); ?>

            <?php
                // Auction Fields
                $meta_auction_item_condition = get_post_meta( $product_id, '_auction_item_condition', true );
                $meta_auction_type = get_post_meta( $product_id, '_auction_type', true );
                $meta_auction_proxy = get_post_meta( $product_id, '_auction_proxy', true );
                $meta_auction_sealed = get_post_meta( $product_id, '_auction_sealed', true );

                $meta_auction_start_price = get_post_meta( $product_id, '_auction_start_price', true );
                $meta_auction_bid_increment = get_post_meta( $product_id, '_auction_bid_increment', true );
                $meta_auction_reserved_price = get_post_meta( $product_id, '_auction_reserved_price', true );
                $meta_regular_price = get_post_meta( $product_id, '_regular_price', true );

                $meta_auction_dates_from = get_post_meta( $product_id, '_auction_dates_from', true );
                $meta_auction_dates_to = get_post_meta( $product_id, '_auction_dates_to', true );
            ?>
            <!-- Auction Settings -->
            <h3><?php esc_html_e( 'Auction Settings', 'ibid' ); ?></h3>
            <div id="auction_tab" class="panel woocommerce_options_panel" style="display: block;">

                <div class="row">
                    <div class="col-md-4">
                        <p class=" form-field _auction_item_condition_field">
                            <label for="_auction_item_condition"><?php esc_html_e( 'Item condition', 'ibid' ); ?></label>
                            <select style="" id="_auction_item_condition" name="_auction_item_condition" class="form-control select short">
                                <option value="new" <?php if($meta_auction_item_condition == 'new'){echo 'selected';} ?>><?php echo esc_html__('New', 'ibid'); ?></option>
                                <option value="used" <?php if($meta_auction_item_condition == 'used'){echo 'selected';} ?>><?php echo esc_html__('Used', 'ibid'); ?></option>
                            </select>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class=" form-field _auction_type_field">
                            <label for="_auction_type"><?php esc_html_e( 'Auction type', 'ibid' ); ?></label>
                            <select id="_auction_type" name="_auction_type" class="form-control select short">
                                <option value="normal" <?php if($meta_auction_type == 'normal'){echo 'selected'; } ?>><?php echo esc_html__('Normal', 'ibid'); ?></option>
                                <option value="reverse" <?php if($meta_auction_type == 'reverse'){echo 'selected'; } ?>><?php echo esc_html__('Reverse', 'ibid'); ?></option>
                            </select>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <p class="form-field _auction_proxy_field ">
                            <label for="_auction_proxy"><?php esc_html_e( 'Proxy bidding?', 'ibid' ); ?></label><span class="woocommerce-help-tip"></span>
                            <input type="checkbox" class="wcfm-checkbox checkbox" style="" name="_auction_proxy" id="_auction_proxy" value="<?php if($meta_auction_proxy == 'yes'){echo esc_attr__('yes', 'ibid');} ?>" <?php if($meta_auction_proxy == 'yes'){echo 'checked';} ?>>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <p class="form-field _auction_sealed_field ">
                            <label for="_auction_sealed"><?php esc_html_e( 'Sealed bidding?', 'ibid' ); ?></label><span class="woocommerce-help-tip"></span>
                            <input type="checkbox" class="wcfm-checkbox checkbox" style="" name="_auction_sealed" id="_auction_sealed" value="<?php if($meta_auction_sealed == 'yes'){echo esc_attr__('yes', 'ibid');} ?>" <?php if($meta_auction_sealed == 'yes'){echo 'checked';} ?>>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="form-field _auction_start_price_field ">
                            <label for="_auction_start_price"><?php esc_html_e( 'Start Price', 'ibid' ); ?> <?php echo esc_html( get_woocommerce_currency_symbol() ); ?></label>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="_auction_start_price" id="_auction_start_price" value="<?php echo esc_attr($meta_auction_start_price); ?>" step="any" min="0">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="form-field _auction_bid_increment_field ">
                            <label for="_auction_bid_increment"><?php esc_html_e( 'Bid increment', 'ibid' ); ?> <?php echo esc_html( get_woocommerce_currency_symbol() ); ?></label>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="_auction_bid_increment" id="_auction_bid_increment" value="<?php echo esc_attr($meta_auction_bid_increment); ?>" step="any" min="0">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="form-field _auction_reserved_price_field ">
                            <label for="_auction_reserved_price"><?php esc_html_e( 'Reserve price (', 'ibid' ); ?><?php echo esc_html( get_woocommerce_currency_symbol() ); ?><?php esc_html_e( ')', 'ibid' ); ?></label><span class="woocommerce-help-tip"></span>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="_auction_reserved_price" id="_auction_reserved_price" value="<?php echo esc_attr($meta_auction_reserved_price); ?>" step="any" min="0">
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="_regular_price_field ">
                            <label for="_regular_price"><?php esc_html_e( 'Buy it now price (', 'ibid' ); ?><?php echo esc_html( get_woocommerce_currency_symbol() ); ?><?php esc_html_e( ') = Regular Price', 'ibid' ); ?></label><span class="woocommerce-help-tip"></span>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="_regular_price" id="_regular_price" value="<?php echo esc_attr($meta_regular_price); ?>">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="auction_dates_fields">
                            <label for="_auction_dates_from"><?php esc_html_e( 'Auction Date - Start', 'ibid' ); ?></label>
                            <input type="text" class="form-control ibid_datetime_picker" name="_auction_dates_from" id="_auction_dates_from" value="<?php echo esc_attr($meta_auction_dates_from); ?>" placeholder="<?php esc_attr_e( 'From… YYYY-MM-DD HH:MM', 'ibid' ); ?>">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="auction_dates_fields">
                            <label for="_auction_dates_to"><?php esc_html_e( 'Auction Date - End', 'ibid' ); ?></label>
                            <input type="text" class="form-control ibid_datetime_picker" name="_auction_dates_to" id="_auction_dates_to" value="<?php echo esc_attr($meta_auction_dates_to); ?>" placeholder="<?php esc_attr_e( 'To… YYYY-MM-DD HH:MM', 'ibid' ); ?>">
                        </p>
                    </div>
                </div>
            </div>

            <?php 
                // RELIST OPTIONS
                $meta_auction_automatic_relist = get_post_meta( $product_id, '_auction_automatic_relist', true );
                $meta_auction_relist_fail_time = get_post_meta( $product_id, '_auction_relist_fail_time', true );
                $meta_auction_relist_not_paid_time = get_post_meta( $product_id, '_auction_relist_not_paid_time', true );
                $meta_auction_relist_duration = get_post_meta( $product_id, '_auction_relist_duration', true );
            ?>
            <!-- Automatic relist auction -->
            <h4 class="ibid-relist-auction"><?php esc_html_e( 'Automatic relist auction', 'ibid' ); ?></h4>
            <div class="row">
                <div class="col-md-4">
                    <p class=" form-field field_auction_automatic_relist">
                        <input type="checkbox" class="wcfm-checkbox wcfm_half_ele_checkbox checkbox" style="" name="_auction_automatic_relist" id="_auction_automatic_relist" value="<?php if($meta_auction_automatic_relist == 'yes'){echo 'yes';} ?>" <?php if($meta_auction_automatic_relist == 'yes'){echo 'checked';} ?>><label for="_auction_automatic_relist"><?php esc_html_e( 'Enable relist auction', 'ibid' ); ?></label>
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <p class=" form-field _auction_item_condition_field">
                        <label for="_auction_relist_fail_time"><?php esc_html_e( 'Relist if fail after n hours', 'ibid' ); ?></label>
                        <input type="number" class="form-control wc_input_price short" style="" name="_auction_relist_fail_time" id="_auction_relist_fail_time" value="<?php echo esc_attr($meta_auction_relist_fail_time); ?>" step="any" min="0">
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <p class=" form-field _auction_item_condition_field">
                        <label for="_auction_relist_not_paid_time"><?php esc_html_e( 'Relist if not paid after n hours', 'ibid' ); ?></label>
                        <input type="number" class="form-control wc_input_price short" style="" name="_auction_relist_not_paid_time" id="_auction_relist_not_paid_time" value="<?php echo esc_attr($meta_auction_relist_not_paid_time); ?>" step="any" min="0">
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <p class=" form-field _auction_item_condition_field">
                        <label for="_auction_item_condition"><?php esc_html_e( 'Relist auction duration in h', 'ibid' ); ?></label>
                        <input type="number" class="form-control wc_input_price short" style="" name="_auction_relist_duration" id="_auction_relist_duration" value="<?php echo esc_attr($meta_auction_relist_duration); ?>" step="any" min="0">
                    </p>
                </div>
            </div>

            <?php do_action('ibid_after_add_auction_form', $product_id); ?>

        </div>

        <?php
    }
}
if(class_exists( 'WooCommerce_simple_auction' )){
    add_action('ibid_dokan_edit_product_before_short_description', 'ibid_custom_auctions_blue_box');
    if (class_exists('WCFM') && !class_exists('WCFMu')) {
        add_action('ibid_wcfm_edit_product_before_tabs', 'ibid_custom_auctions_blue_box');
    }
    add_action('ibid_wcmp_auctions_tab_content', 'ibid_custom_auctions_blue_box');
}


/**
*
* Adds the blue auctions box to marketplace plugins such as dokan/wcfm
*
* @since 3.3
*
* @package ibid
*/
if (!function_exists('ibid_uaplugin_custom_auctions_blue_box')) {
    function ibid_uaplugin_custom_auctions_blue_box($product_id){

        $style = 'display: none;';
        if ($product_id) {
            $_product = wc_get_product( $product_id );
            if( $_product->is_type( 'auction' ) ) {
                $style = 'display: block;';
            }
        }

        ?>

        <div class="ibid-auction-settings" style="<?php echo esc_attr($style); ?>">

            <?php do_action('ibid_before_add_auction_form', $product_id); ?>

            <?php
                // Auction Fields
                $meta_auction_item_condition = get_post_meta( $product_id, 'woo_ua_product_condition', true );

                $meta_auction_start_price = get_post_meta( $product_id, 'woo_ua_opening_price', true );
                $meta_auction_bid_increment = get_post_meta( $product_id, 'woo_ua_bid_increment', true );
                $meta_auction_reserved_price = get_post_meta( $product_id, 'woo_ua_lowest_price', true );
                $meta_regular_price = get_post_meta( $product_id, '_regular_price', true );

                $meta_auction_dates_to = get_post_meta( $product_id, 'woo_ua_auction_end_date', true );
            ?>
            <!-- Auction Settings -->
            <h3><?php esc_html_e( 'Auction Settings', 'ibid' ); ?></h3>
            <div id="auction_tab" class="panel woocommerce_options_panel" style="display: block;">

                <div class="row">
                    <div class="col-md-4">
                        <p class=" form-field _auction_item_condition_field">
                            <label for="_auction_item_condition"><?php esc_html_e( 'Item condition', 'ibid' ); ?></label>
                            <select style="" id="_auction_item_condition" name="woo_ua_product_condition" class="form-control select short">
                                <option value="new" <?php if($meta_auction_item_condition == 'new'){echo 'selected';} ?>><?php esc_html_e( 'New', 'ibid' ); ?></option>
                                <option value="used" <?php if($meta_auction_item_condition == 'used'){echo 'selected';} ?>><?php esc_html_e( 'Used', 'ibid' ); ?></option>
                            </select>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="form-field _auction_start_price_field ">
                            <label for="_auction_start_price"><?php esc_html_e( 'Start Price', 'ibid' ); ?> <?php echo esc_html( get_woocommerce_currency_symbol() ); ?></label>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="woo_ua_opening_price" id="_auction_start_price" value="<?php echo esc_attr($meta_auction_start_price); ?>" step="any" min="0">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="form-field _auction_bid_increment_field ">
                            <label for="_auction_bid_increment"><?php esc_html_e( 'Bid increment', 'ibid' ); ?> <?php echo esc_html( get_woocommerce_currency_symbol() ); ?></label>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="woo_ua_bid_increment" id="_auction_bid_increment" value="<?php echo esc_attr($meta_auction_bid_increment); ?>" step="any" min="0">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="form-field _auction_reserved_price_field ">
                            <label for="_auction_reserved_price"><?php esc_html_e( 'Reserve price (', 'ibid' ); ?><?php echo esc_html( get_woocommerce_currency_symbol() ); ?><?php esc_html_e( ')', 'ibid' ); ?></label><span class="woocommerce-help-tip"></span>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="woo_ua_lowest_price" id="_auction_reserved_price" value="<?php echo esc_attr($meta_auction_reserved_price); ?>" step="any" min="0">
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p class="_regular_price_field ">
                            <label for="_regular_price"><?php esc_html_e( 'Buy it now price (', 'ibid' ); ?><?php echo esc_html( get_woocommerce_currency_symbol() ); ?><?php esc_html_e( ') = Regular Price', 'ibid' ); ?></label><span class="woocommerce-help-tip"></span>
                            <input type="text" class="form-control wc_input_price short wc_input_price" style="" name="_regular_price" id="_regular_price" value="<?php echo esc_attr($meta_regular_price); ?>">
                        </p>
                    </div>
                    <div class="col-md-4">
                        <p class="auction_dates_fields">
                            <label for="_auction_dates_to"><?php esc_html_e( 'Auction Date - End', 'ibid' ); ?></label>
                            <input type="text" class="form-control ibid_datetime_picker" name="woo_ua_auction_end_date" id="_auction_dates_to" value="<?php echo esc_attr($meta_auction_dates_to); ?>" placeholder="<?php esc_attr_e( 'To… YYYY-MM-DD HH:MM', 'ibid' ); ?>">
                        </p>
                    </div>
                </div>
            </div>

            <?php do_action('ibid_after_add_auction_form', $product_id); ?>

        </div>

        <?php
    }
}
if(class_exists('Ultimate_WooCommerce_Auction_Pro')){
    add_action('ibid_dokan_edit_product_before_short_description', 'ibid_uaplugin_custom_auctions_blue_box');
}
if(class_exists('Ultimate_WooCommerce_Auction_Free')){
    add_action('ibid_dokan_edit_product_before_short_description', 'ibid_uaplugin_custom_auctions_blue_box');
    add_action('ibid_wcfm_edit_product_before_tabs', 'ibid_uaplugin_custom_auctions_blue_box');
    add_action('ibid_wcmp_auctions_tab_content', 'ibid_uaplugin_custom_auctions_blue_box');
}